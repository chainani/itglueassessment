#!/bin/bash
cd docker/app
docker-compose up -d
docker exec app composer install -d /var/www/MortgageCalculator
