# Mortgage Calculator

### Requirements
1. [Docker](https://docs.docker.com/install/)
2. [Docker Compose](https://docs.docker.com/compose/install/)

### Deployment Instructions:

1. Clone repository on to your computer
2. Navigate to the repository in terminal
3. Run the init script : ` sh ./init.sh`

### Usage:
Upon deployment the swagger documentation for the APIs will be available at [http://localhost:8080/](http://localhost:8080/)

### Running Tests:

1. Start bash in docker container: `docker exec -it app /bin/bash`
2. Navigate to the `MortgageCalculator` directory: `cd MortgageCalculator`
3. Run laravel test command: `php artisan test`

You can also use this command to run all tests instead: `docker exec -w /var/www/MortgageCalculator app php artisan test `

Example output:
![](images/Testing.png)


#### **Feel free to reach out with any questions or comments**