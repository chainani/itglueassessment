<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\JsonResponse;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    /**
     * @OA\Info(
     *      version="1.0.0",
     *      title="Mortgage Calculator Swagger",
     *      description="Swagger to access the mortgage calculator API"
     * )
     */

    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * @param array|null $data
     * @param array|null $errors
     * @param int $status
     * @return JsonResponse
     */
    protected function getJsonResponse($data=null, $errors=null, $status=200){
        $response=['success'=>empty($errors)];
        if($errors){
            $response['errors']= $errors;
        }
        if($data){
            $response['data']=$data;
        }
        return new JsonResponse($response,$status);
    }
}
