<?php

namespace App\Http\Controllers;

use App\InterestRate;
use App\Mortgage;
use App\Payments;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class MortgageController extends Controller
{

    /**
     * @OA\Get(
     *      path="/payment-amount",
     *      operationId="getPaymentAmount",
     *      tags={"/api/v1"},
     *      summary="Get the recurring payment amount of a mortgage",
     *      @OA\Parameter(
     *          name="askingPrice",
     *          description="Asking Price",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="number",
     *              minimum=0,
     *              example=125000
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="downPayment",
     *          description="Down Payment",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="number",
     *              minimum=0,
     *              example=25000
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="paymentSchedule",
     *          description="Payment Schedule",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="string",
     *              enum={"weekly","biweekly","monthly"},
     *              example="monthly"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="amortizationPeriod",
     *          description="Amortization Period (in years)",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="integer",
     *              minimum=5,
     *              maximum=25,
     *              example=15
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="successful operation",
     *          @OA\MediaType(
     *              mediaType="application/json",
     *              @OA\Schema(
     *                  type="object",
     *                  required={"success"},
     *                  @OA\Property(
     *                      property="success",
     *                      type="boolean",
     *                      example=true
     *                  ),
     *                  @OA\Property(
     *                      property="data",
     *                      description= "Response Data",
     *                      type="object",
     *                      @OA\Property(
     *                          property="mortgage",
     *                          type="object",
     *                          @OA\Property(
     *                              property="askingPrice",
     *                              type="number",
     *                              example=125000
     *                          ),
     *                          @OA\Property(
     *                              property="downPayment",
     *                              type="number",
     *                              example=25000
     *                          ),
     *                          @OA\Property(
     *                              property="insuranceRate",
     *                              type="number",
     *                              example=0
     *                          ),
     *                          @OA\Property(
     *                              property="insuranceAmount",
     *                              type="number",
     *                              example=0
     *                          ),
     *                          @OA\Property(
     *                              property="totalMortgage",
     *                              type="number",
     *                              example=100000
     *                          )
     *                      ),
     *                      @OA\Property(
     *                          property="interestRate",
     *                          type="string",
     *                          example="2.50% p.a."
     *                      ),
     *                      @OA\Property(
     *                          property="paymentAmount",
     *                          type="number",
     *                          example=666.79
     *                      ),
     *                      @OA\Property(
     *                          property="paymentSchedule",
     *                          type="string",
     *                          enum={"weekly","biweekly","monthly"},
     *                          example="monthly"
     *                      ),
     *                      @OA\Property(
     *                          property="amortizationPeriod",
     *                          type="string",
     *                          example="15 years"
     *                      )
     *                  )
     *              )
     *          )
     *       ),
     *       @OA\Response(
     *          response=400,
     *          description="Bad request"
     *       )
     *     )
     * @param Request $request
     * @return JsonResponse
     */
    public function getPaymentAmount(Request $request){
        try {
            $this->validate($request,
                [
                    'askingPrice' => 'required|numeric|gte:0.01',
                    'downPayment' => [
                        'required',
                        'numeric',
                        'gte:0.01',
                        function ($attribute, $value, $fail) use ($request) {
                            $askingPrice = $request->get('askingPrice');
                            $minimumDown=(float)bcmul(min([$askingPrice,500000]),0.05,2);
                            if($askingPrice>500000){
                                $minimumDown=(float)bcadd($minimumDown, bcmul(bcsub($askingPrice,500000,2),0.1,2),2);
                            }

                            if($value<$minimumDown){
                                $fail("The down payment must be at least $minimumDown for asking price: $askingPrice.");
                            }
                        }
                    ],
                    'paymentSchedule' => 'required|in:weekly,biweekly,monthly',
                    'amortizationPeriod' => 'required|int|gte:5|lte:25'
                ]
            );
            $askingPrice = $request->get('askingPrice');
            $downPayment = $request->get('downPayment');
            $paymentSchedule = $request->get('paymentSchedule');
            $amortizationPeriodInYears = $request->get('amortizationPeriod');
            $mortgage = new Mortgage($askingPrice, $downPayment);
            $paymentAmount = Payments::calculatePayment($mortgage->getTotalMortgage(), $paymentSchedule, $amortizationPeriodInYears);
            return $this->getJsonResponse(
                [
                    'mortgage' => $mortgage->toArray(),
                    'interestRate'=> InterestRate::getRateString(),
                    'paymentAmount' => $paymentAmount,
                    'paymentSchedule' => $paymentSchedule,
                    'amortizationPeriod' => $amortizationPeriodInYears." years"
                ]
            );
        } catch (ValidationException $e) {
            return $this->getJsonResponse([],$e->validator->getMessageBag()->all(),422);
        }
    }

    /**
     * @OA\Get(
     *      path="/mortgage-amount",
     *      operationId="getMaxMortgageAmount",
     *      tags={"/api/v1"},
     *      summary="Get the maximum mortgage amount (principal)",
     *      @OA\Parameter(
     *          name="paymentAmount",
     *          description="Payment Amount",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="number",
     *              minimum=0,
     *              example=666.79
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="paymentSchedule",
     *          description="Payment Schedule",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="string",
     *              enum={"weekly","biweekly","monthly"},
     *              example="monthly"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="amortizationPeriod",
     *          description="Amortization Period (in years)",
     *          required=true,
     *          in="query",
     *          @OA\Schema(
     *              type="integer",
     *              minimum=5,
     *              maximum=25,
     *              example=15
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="successful operation",
     *          @OA\MediaType(
     *              mediaType="application/json",
     *              @OA\Schema(
     *                  type="object",
     *                  required={"success"},
     *                  @OA\Property(
     *                      property="success",
     *                      type="boolean",
     *                      example=true
     *                  ),
     *                  @OA\Property(
     *                      property="data",
     *                      description= "Response Data",
     *                      type="object",
     *                      @OA\Property(
     *                          property="mortgageAmount",
     *                          type="number",
     *                          example=100000.3
     *                      ),
     *                      @OA\Property(
     *                          property="interestRate",
     *                          type="string",
     *                          example="2.50% p.a."
     *                      ),
     *                      @OA\Property(
     *                          property="paymentAmount",
     *                          type="number",
     *                          example=666.79
     *                      ),
     *                      @OA\Property(
     *                          property="paymentSchedule",
     *                          type="string",
     *                          enum={"weekly","biweekly","monthly"},
     *                          example="monthly"
     *                      ),
     *                      @OA\Property(
     *                          property="amortizationPeriod",
     *                          type="string",
     *                          example="15 years"
     *                      )
     *                  )
     *              )
     *          )
     *       ),
     *       @OA\Response(
     *          response=400,
     *          description="Bad request"
     *       )
     *     )
     *
     */
    public function getMaxMortgageAmount(Request $request){
        try {
            $this->validate($request,
                [
                    'paymentAmount' => 'required|numeric|gte:0.01',
                    'paymentSchedule' => 'required|in:weekly,biweekly,monthly',
                    'amortizationPeriod' => 'required|int|gte:5|lte:25'
                ]
            );
            $paymentAmount = $request->get('paymentAmount');
            $paymentSchedule = $request->get('paymentSchedule');
            $amortizationPeriodInYears = $request->get('amortizationPeriod');
            $maxMortgage = Payments::calculateMaxMortgage($paymentAmount, $paymentSchedule, $amortizationPeriodInYears);
            return $this->getJsonResponse(
                [
                    'totalMortgage' => $maxMortgage,
                    'interestRate'=> InterestRate::getRateString(),
                    'paymentAmount' => $paymentAmount,
                    'paymentSchedule' => $paymentSchedule,
                    'amortizationPeriod' => $amortizationPeriodInYears." years"
                ]
            );
        } catch (ValidationException $e) {
            return $this->getJsonResponse([],$e->validator->getMessageBag()->all(),422);
        }
    }

    /**
     * @OA\Patch(
     *      path="/interest-rate",
     *      operationId="updateInterestRate",
     *      tags={"/api/v1"},
     *      summary="Change the interest rate used by the application",
     *      @OA\RequestBody(
     *          description="body params",
     *          required=true,
     *          @OA\MediaType(
     *              mediaType="application/json",
     *              @OA\Schema(
     *                  type="object",
     *                  required={"rate"},
     *                  @OA\Property(
     *                      property="interestRate",
     *                      description= "Interest Rate",
     *                      type="number",
     *                      minimum=0,
     *                      example=1
     *                  )
     *              )
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="successful operation",
     *          @OA\MediaType(
     *              mediaType="application/json",
     *              @OA\Schema(
     *                  type="object",
     *                  required={"success"},
     *                  @OA\Property(
     *                      property="success",
     *                      type="boolean",
     *                      example=true
     *                  ),
     *                  @OA\Property(
     *                      property="data",
     *                      description= "Response Data",
     *                      type="object",
     *                      @OA\Property(
     *                          property="oldInterestRate",
     *                          type="string",
     *                          example="2.50% p.a."
     *                      ),
     *                      @OA\Property(
     *                          property="currentInterestRate",
     *                          type="string",
     *                          example="1.00% p.a."
     *                      )
     *                  )
     *              )
     *          )
     *       ),
     *       @OA\Response(
     *          response=400,
     *          description="Bad request"
     *       )
     *     )
     *
     */
    public function updateInterestRate(Request $request){
        try {
            $this->validate($request,
                [
                    'interestRate' => 'required|numeric|gte:0.01'
                ]
            );
            $oldRate = InterestRate::getRateString();
            InterestRate::setInterestRate($request->get('interestRate'));
            return $this->getJsonResponse(
                [
                    'oldInterestRate'=> $oldRate,
                    'currentInterestRate' => InterestRate::getRateString()
                ]
            );
        } catch (ValidationException $e) {
            return $this->getJsonResponse([],$e->validator->getMessageBag()->all(),422);
        }
    }


}
