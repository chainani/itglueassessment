<?php


namespace App;


class Mortgage
{
    private $askingPrice;
    private $downPayment;
    private $insuranceAmount;
    private $insuranceRate;
    private $totalMortgage;
    public function __construct(float $askingPrice, float $downPayment)
    {

        $this->askingPrice = $askingPrice;
        $this->downPayment = $downPayment;
        $this->setInsuranceRate();
        $this->setInsuranceAmount();
        $this->setTotalMortgage();

    }

    /**
     * @return mixed
     */
    public function getTotalMortgage()
    {
        return $this->totalMortgage;
    }

    /**
     */
    private function setInsuranceRate(): void
    {
        if($this->askingPrice>1000000){
            $this->insuranceRate=0;
            return;
        }
        $downPercent= bcmul(bcdiv($this->downPayment, $this->askingPrice,4),100);
        switch (true){
            case $downPercent<10:
                $this->insuranceRate=3.15;
                break;
            case $downPercent<15:
                $this->insuranceRate=2.4;
                break;
            case $downPercent<20:
                $this->insuranceRate=1.8;
                break;
            default:
                $this->insuranceRate=0;
        }
    }

    /**
     */
    private function setInsuranceAmount(): void
    {
        $this->insuranceAmount = (float)bcmul(bcsub($this->askingPrice,$this->downPayment), bcdiv($this->insuranceRate,100,4),2);
    }

    /**
     */
    private function setTotalMortgage(): void
    {
        $this->totalMortgage = (float)bcadd(bcsub($this->askingPrice,$this->downPayment), $this->insuranceAmount, 2);
    }

    public function toArray()
    {
        return [
            'askingPrice'=>$this->askingPrice,
            'downPayment'=>$this->downPayment,
            'insuranceRate'=>$this->insuranceRate,
            'insuranceAmount'=>$this->insuranceAmount,
            'totalMortgage'=>$this->totalMortgage
        ];
    }

}
