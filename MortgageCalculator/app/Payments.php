<?php


namespace App;


class Payments
{
    public static function calculatePayment(float $totalMortgage, $paymentSchedule, int $amortizationPeriod):float {
        $unRoundedPayment = bcmul(
            $totalMortgage,
            self::getPaymentMultiplier($paymentSchedule, $amortizationPeriod),
            8
        );
        return round($unRoundedPayment,2);//rounding to 2 decimal points

    }

    public static function calculateMaxMortgage(float $paymentAmount, $paymentSchedule, int $amortizationPeriod):float {
        $unRoundedMortgage = bcdiv(
            $paymentAmount,
            self::getPaymentMultiplier($paymentSchedule, $amortizationPeriod),
            8
        );
        return round($unRoundedMortgage,2);//rounding to 2 decimal points

    }

    /**
     * @param $paymentSchedule
     * @param int $amortizationPeriod
     * @return string|null
     */
    private static function getPaymentMultiplier($paymentSchedule, int $amortizationPeriod)
    {
        $rate = InterestRate::$paymentSchedule();
        $payments = self::$paymentSchedule($amortizationPeriod);
        $onePlusRtoNth = bcpow(bcadd(1, $rate, 8), $payments, 8);//(1+r)^n1
        return bcdiv(
            bcmul(
                $rate,
                $onePlusRtoNth,
                8),
            bcsub(
                $onePlusRtoNth,
                1,
                8),
            8
        );
    }

    /** @noinspection PhpUnusedPrivateMethodInspection */
    private static function monthly(int $durationInYears ):int{
        return 12*$durationInYears;
    }

    /** @noinspection PhpUnusedPrivateMethodInspection */
    private static function weekly(int $durationInYears ):int{
        return 52*$durationInYears;
    }

    /** @noinspection PhpUnusedPrivateMethodInspection */
    private static function biweekly(int $durationInYears ):int{
        return 26*$durationInYears;
    }

}
