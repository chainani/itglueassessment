<?php


namespace App;



use Illuminate\Support\Facades\Cache;

class InterestRate
{
    const RATE_KEY="interestRate";

    /** @noinspection PhpUnused */
    public static function monthly():float {
        return bcdiv(self::getAnnualRate(),12,8);
    }

    /** @noinspection PhpUnused */
    public static function weekly():float {
        return bcdiv(self::getAnnualRate(),52,8);

    }

    /** @noinspection PhpUnused */
    public static function biweekly():float {
        return bcdiv(self::getAnnualRate(),26,8);
    }

    /**
     * @return string
     */
    public static function getRateString(): string
    {

        return (bcmul(self::getAnnualRate(), 100, 2)) . '% p.a.';
    }
    public static function setInterestRate(float $rate):void {
        $rate= bcdiv(round($rate,2),100,4);
        Cache::forever(self::RATE_KEY,$rate);
    }

    /**
     * @return float
     */
    public static function getAnnualRate(): float
    {
        return Cache::get(self::RATE_KEY, 0.025);
    }
}
