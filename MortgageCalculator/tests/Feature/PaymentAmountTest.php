<?php

namespace Tests\Feature;

use Tests\TestCase;

class PaymentAmountTest extends TestCase
{
    public function testValidInput()
    {
        //initializing interest rate;
        \App\InterestRate::setInterestRate(2.5);
        $askingPrice = 125000;
        $downPayment = 25000;
        $paymentSchedule = "monthly";
        $amortizationPeriod = 15;
        $response = $this->getPaymentAmountResponse($askingPrice, $downPayment, $paymentSchedule, $amortizationPeriod);
        $response->assertStatus(200)->assertJson(
            [
                'success'=> true,
                'data'=> [
                    'mortgage'=> [
                        'askingPrice'=> 125000,
                        'downPayment'=> 25000,
                        'insuranceRate'=> 0,
                        'insuranceAmount'=> 0,
                        'totalMortgage'=> 100000
                    ],
                    'interestRate'=> '2.50% p.a.',
                    'paymentAmount'=> 666.79,
                    'paymentSchedule'=> 'monthly',
                    'amortizationPeriod'=> '15 years'
                ]
            ]
        );

    }


    public function testInadequateDownPayment()
    {
        $askingPrice = 125000;
        $downPayment = 5000;
        $paymentSchedule = "monthly";
        $amortizationPeriod = 15;
        $response = $this->getPaymentAmountResponse($askingPrice, $downPayment, $paymentSchedule, $amortizationPeriod);
        $response->assertStatus(422)->assertJson(
            [
                'success'=> false,
                "errors"=> [
                    "The down payment must be at least 6250 for asking price: 125000."
                ]
            ]
        );

    }

    public function testInvalidAmortizationPeriod()
    {
        $askingPrice = 125000;
        $downPayment = 25000;
        $paymentSchedule = "monthly";
        $amortizationPeriod = 1;
        $response = $this->getPaymentAmountResponse($askingPrice, $downPayment, $paymentSchedule, $amortizationPeriod);
        $response->assertStatus(422)->assertJson(
            [
                'success'=> false,
                "errors"=> [
                    "The amortization period must be greater than or equal 5."
                ]
            ]
        );

        $amortizationPeriod = 35;
        $response = $this->getPaymentAmountResponse($askingPrice, $downPayment, $paymentSchedule, $amortizationPeriod);
        $response->assertStatus(422)->assertJson(
            [
                'success'=> false,
                "errors"=> [
                    "The amortization period must be less than or equal 25."
                ]
            ]
        );

    }

    /**
     * @param $askingPrice
     * @param $downPayment
     * @param $paymentSchedule
     * @param $amortizationPeriod
     * @return \Illuminate\Testing\TestResponse
     */
    public function getPaymentAmountResponse($askingPrice, $downPayment, $paymentSchedule, $amortizationPeriod): \Illuminate\Testing\TestResponse
    {
        return $this->get("/api/v1/payment-amount?askingPrice={$askingPrice}&downPayment={$downPayment}&paymentSchedule={$paymentSchedule}&amortizationPeriod={$amortizationPeriod}");
    }
}
