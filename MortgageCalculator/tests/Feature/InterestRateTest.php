<?php

namespace Tests\Feature;

use Tests\TestCase;

class InterestRateTest extends TestCase
{
    public function testValidInput()
    {
        //initializing interest rate;
        \App\InterestRate::setInterestRate(2.5);
        $interestRate = 1;
        $response = $this->getInterestRateResponse($interestRate);
        $response->assertStatus(200)->assertJson(
            [
                'success'=> true,
                'data'=> [
                    'oldInterestRate'=> '2.50% p.a.',
                    'currentInterestRate'=> '1.00% p.a.'
                ]
            ]
        );

    }


    public function testInvalidInterestRate()
    {
        $interestRate = -1;
        $response = $this->getInterestRateResponse($interestRate);
        $response->assertStatus(422)->assertJson(
            [
                'success'=> false,
                "errors"=> [
                    "The interest rate must be greater than or equal 0.01."
                ]
            ]
        );

    }

    public function testPostCall(){
        \App\InterestRate::setInterestRate(2.5);
        $interestRate = 1;
        $response = $this->getInterestRateResponse($interestRate, "POST");
        $response->assertStatus(405);

    }

    /**
     * @param $interestRate
     * @param $method
     * @return \Illuminate\Testing\TestResponse
     */
    public function getInterestRateResponse($interestRate, $method="Patch"): \Illuminate\Testing\TestResponse
    {
        return $this->json($method,"/api/v1/interest-rate",['interestRate'=>$interestRate]);
    }
}
