<?php

namespace Tests\Feature;

use Tests\TestCase;

class MaxMortgageTest extends TestCase
{
    public function testValidInput()
    {
        //initializing interest rate;
        \App\InterestRate::setInterestRate(2.5);
        $paymentAmount = 666.79;
        $paymentSchedule = "monthly";
        $amortizationPeriod = 15;
        $response = $this->getMaxMortgageResponse($paymentAmount, $paymentSchedule, $amortizationPeriod);
        $response->assertStatus(200)->assertJson(
            [
                'success'=> true,
                'data'=> [
                    'totalMortgage'=> 100000.3,
                    'interestRate'=> '2.50% p.a.',
                    'paymentAmount'=> 666.79,
                    'paymentSchedule'=> 'monthly',
                    'amortizationPeriod'=> '15 years'
                ]
            ]
        );

    }


    public function testInvalidPayment()
    {
        $paymentAmount = -1;
        $paymentSchedule = "monthly";
        $amortizationPeriod = 15;
        $response = $this->getMaxMortgageResponse($paymentAmount, $paymentSchedule, $amortizationPeriod);
        $response->assertStatus(422)->assertJson(
            [
                'success'=> false,
                "errors"=> [
                    "The payment amount must be greater than or equal 0.01."
                ]
            ]
        );

    }

    public function testInvalidAmortizationPeriod()
    {
        $paymentAmount = 666.79;
        $paymentSchedule = "monthly";
        $amortizationPeriod = 1;
        $response = $this->getMaxMortgageResponse($paymentAmount, $paymentSchedule, $amortizationPeriod);
        $response->assertStatus(422)->assertJson(
            [
                'success'=> false,
                "errors"=> [
                    "The amortization period must be greater than or equal 5."
                ]
            ]
        );

        $amortizationPeriod = 35;
        $response = $this->getMaxMortgageResponse($paymentAmount, $paymentSchedule, $amortizationPeriod);
        $response->assertStatus(422)->assertJson(
            [
                'success'=> false,
                "errors"=> [
                    "The amortization period must be less than or equal 25."
                ]
            ]
        );

    }

    /**
     * @param $paymentAmount
     * @param $paymentSchedule
     * @param $amortizationPeriod
     * @return \Illuminate\Testing\TestResponse
     */
    public function getMaxMortgageResponse($paymentAmount, $paymentSchedule, $amortizationPeriod): \Illuminate\Testing\TestResponse
    {
        return $this->get("/api/v1/mortgage-amount?paymentAmount={$paymentAmount}&paymentSchedule={$paymentSchedule}&amortizationPeriod={$amortizationPeriod}");
    }
}
